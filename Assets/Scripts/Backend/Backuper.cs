﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Timers;
using UnityEngine;

namespace Backend {
    public class Backuper {
        public string fileName;
        
        /// <summary>
        /// The file Ending of the selected file
        /// </summary>
        public string fileEndung;
        
        /// <summary>
        /// The contens of the selected File. This will be written to the new file on Change
        /// </summary>
        private string fileInhalt;
        private DateTime fileAnderung;
        private int dopplungen;
        private readonly Timer timer;
        private bool inited;
        private static Backuper locInstance;

        private Backuper() {
            timer = new Timer{AutoReset = true, Interval = 0.1f, Enabled = false};
            inited = false;
            dopplungen = 0;
        }

        public void init() {
            if (fileName == null || fileEndung == null || timer == null || inited) {
                Debug.LogError("Failed to start system.");
                return;
            }
            fileAnderung = new FileInfo(fileName).LastWriteTime;
            fileInhalt = File.ReadAllText(fileName + fileEndung);
            inited = true;
        }
        
        public void setTimeOut(int timeOutMin) => timer.Interval = timeOutMin * 1000 * 60;

        public bool startBackuping() {
            if (!inited) {
                Debug.LogError("Setup with fileName to watch required before start.");
                return false;
            }
            if (timer.Interval <= 0.1f) {
                Debug.LogError("Please set a valid lookup intervall.");
                return false;
            }
            try{
                backup(true);
                timer.Elapsed += backup;
                timer.Start();
                return true;
            } catch(Exception e) {
                Debug.LogException(e);
                if (timer.Enabled)
                    timer.Close();
                return false;
            }
        }

        public bool stopBackuping() {
            try {
                timer.Stop();
                backup(true);
                inited = false;
                return true;
            } catch(Exception e) {
               Debug.LogException(e);
               try {
                   timer.Stop();
               } catch(Exception w) {
                Debug.LogException(w);
               }
               return false;
            }
        }
        
        private void backup(object sender, ElapsedEventArgs e) => backup(false);

        public void backup(bool skipCheck) {
            if (!inited) {
                Debug.LogError("System is not prepared yet. Please prepare the system first.");
                return;
            }
            if (fileAnderung.Equals(new FileInfo(fileName + fileEndung).LastWriteTime) && !skipCheck) {
                Debug.Log("No Backup needed, because File did not change since last lookup.");
                return;
            }
            int i = 0;
            while ((File.Exists(fileName + " Backup " + dopplungen + fileEndung)
                    || (dopplungen == 0 && File.Exists(fileName + " Backup" + fileEndung))) && i < 1000) {dopplungen++; i++;}
            string newFilename = fileName + " Backup" + (dopplungen == 0 ? "" : " " + dopplungen) + fileEndung;
            File.WriteAllText(newFilename, fileInhalt);
            if (!File.Exists(fileName + fileEndung)) {
                Debug.LogWarning("Original File does not exist anymore. Restoring from last copy.");
                File.WriteAllText(fileName + fileEndung, fileInhalt);
            }
            fileAnderung = new FileInfo(fileName + fileEndung).LastWriteTime;
            Debug.Log("Successfully made Backup " + newFilename.Substring(newFilename.LastIndexOf('\\') + 1) + ".");
            if (skipCheck) return;
            Debug.Log("Now backuping new content of the File.");
            fileInhalt = File.ReadAllText(fileName + fileEndung);

        }
        
        ~Backuper() {
            if (timer.Enabled)
                timer.Stop();
            timer.Close();
            timer.Dispose();
            if (Application.isPlaying)
                backup(false);
            fileInhalt = "";
            fileName = "";
            fileEndung = "";
            dopplungen = 0;
        }

        public static Backuper getInstance() {
            if (locInstance != null)
                return locInstance;
            return (locInstance = new Backuper());
        }
        
        public Timer getTimer() => timer;
        
        public string getFilePath() => fileName;
    }
}
﻿using Backend;
using UnityEngine;
using UnityEngine.UI;

namespace GUI_Skripte {
    public class StartStop : MonoBehaviour {
        
        public Button start, stop;
        public Image runningToggle;
        private static bool isRunning;
        private Backuper backend;

        private void Start() {
            isRunning = false;
            backend = Backuper.getInstance();
            if (runningToggle == null)
                Debug.LogWarning("Running Toggle nicht zugewiesen.");
            if (start != null && stop != null)
                return;
            Debug.LogWarning("Einer der Knopfe ist nicht gesetzt du Honk");
            start = gameObject.name.Equals("Start") ? gameObject.GetComponent<Button>() : gameObject.transform.parent.Find("Start").gameObject.GetComponent<Button>();
            stop = gameObject.name.Equals("Stop") ? gameObject.GetComponent<Button>() : gameObject.transform.parent.Find("Stop").gameObject.GetComponent<Button>();
        }
        public void ToggleRunning(bool shouldStart) {
            if ((shouldStart && isRunning) || (!shouldStart && !isRunning)) {
                Debug.LogError("Wrong input. Can not start already running program or stop non running one.");
                return;
            }
            if (backend.fileName == null) {
                Debug.LogError("No File entered. Please select file via 'Open File'");
                return;
            }
            if(shouldStart) {
                backend.init();
                if (!backend.startBackuping() || !backend.getTimer().Enabled) {
                    Debug.LogError("Failed to start system.");
                    return;
                }
                start.interactable = false;
                stop.interactable = true;
                runningToggle.enabled = true;
                isRunning = true;
                Debug.Log("Sucessfully started System. First check in " + (backend.getTimer().Interval / 1000 / 60) + "Min.");
            }
            else {
                if(!backend.stopBackuping()) {
                    Debug.LogError("System Shutdown failed.");
                    return;
                }
                start.interactable = true;
                stop.interactable = false;
                runningToggle.enabled = false;
                isRunning = false;
                Debug.Log("Sucessfully stopped System.");
                if (runningToggle == null)
                    return;
                runningToggle.enabled = false;
            }
        }
    }
}
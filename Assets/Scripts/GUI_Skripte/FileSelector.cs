﻿using System;
using System.Windows.Forms;
using Backend;
using UnityEngine;
using UnityEngine.UI;

namespace GUI_Skripte {
    public class FileSelector : MonoBehaviour {
        private OpenFileDialog auswahler;
        private string defaultSelectedFileText;
        private string defaultSelectedPathText;
        [SerializeField]
        private Text FileTextObj;
        [SerializeField]
        private Text PathTextObj;

        private void Start() {
            auswahler = new OpenFileDialog {
                Title = "Choose the file to make backups of", 
                AddExtension = false, 
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString(),
                Multiselect = false,
                DereferenceLinks = false
            };
            if (FileTextObj == null) {
                Debug.LogWarning("Und jetzt hast du auch noch vergessen, das TextObjekt für die Datei der Dateianzeige zu setzen. " +
                                 "Man ej, was kannst du eigentlich???");
                return;
            }
            if (PathTextObj == null) {
                Debug.LogWarning("Und jetzt hast du auch noch vergessen, das TextObjekt für den Pfad für die Dateianzeige zu setzen. " +
                                 "Man ej, was kannst du eigentlich???");
                return;
            }
            defaultSelectedFileText = FileTextObj.text; 
            FileTextObj.text += "None";
            defaultSelectedPathText = PathTextObj.text; 
            PathTextObj.text += "None";
        }

        public void selectFile() {
            DialogResult status = auswahler.ShowDialog();
            if (status != DialogResult.OK || auswahler.FileName == null) {
                if (status != DialogResult.Cancel)
                    Debug.LogError("Fehler beim Setzen der Datei");
                return;
            }
            Backuper backend = Backuper.getInstance();
            
            backend.fileName = auswahler.FileName.Substring(0, auswahler.FileName.LastIndexOf('.'));
            backend.fileEndung = auswahler.FileName.Substring(auswahler.FileName.LastIndexOf('.'));
            
            FileTextObj.text = defaultSelectedFileText;
            FileTextObj.text += backend.fileName.Substring(backend.fileName.LastIndexOf('\\') + 1) + backend.fileEndung;
            
            PathTextObj.text = defaultSelectedPathText;
            PathTextObj.text += backend.fileName.Substring(0, backend.fileName.LastIndexOf('\\'));
        }
    }
}
﻿using Backend;
using UnityEngine;
using UnityEngine.UI;

namespace GUI_Skripte {
    public class Slideranzeige : MonoBehaviour {
        public Text anzeige;
        public Slider einsteller; 
        private string defaultText;

        private void Start() {
            if (anzeige == null) {
                Debug.LogWarning("Du dummer Vollhorst hast mal wieder vergessen, die Scheiss Anzeige vernünftig zu setzen!!!");
                anzeige = gameObject.transform.parent.Find("Anzeige").Find("Text").gameObject.GetComponent<Text>();
            }
            if (einsteller == null) {
                Debug.LogWarning("Du dummer Vollhorst hast mal wieder vergessen, den behinderten Slider vernünftig zu setzen!!!");
                einsteller = gameObject.GetComponent<Slider>();
            }
            
            defaultText = anzeige.text;
            Backuper.getInstance().setTimeOut((int)einsteller.value);
            anzeige.text += (Backuper.getInstance().getTimer().Interval / 1000 / 60) + "Min";
        }
        
        public void OnSliderMoved() {
            Backuper.getInstance().setTimeOut((int)einsteller.value);
            anzeige.text = defaultText;
            anzeige.text += (Backuper.getInstance().getTimer().Interval / 1000 / 60) + "Min";
        }
        
    }
}
﻿using UnityEngine;

namespace GUI_Skripte {
    public class ExitKnopf : MonoBehaviour {
        
        public void OnQuit() {
            Application.Quit();
            
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #endif
        }
    }
}
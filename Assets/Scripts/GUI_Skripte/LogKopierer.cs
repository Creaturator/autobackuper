﻿using System;
using System.Linq;
using System.Text;
using NUnit.Framework.Constraints;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace GUI_Skripte {
    public class LogKopierer : MonoBehaviour {
        
        private Text textObj;
        private NativeList<char> newLogs;

        private void Start() {
            textObj = GameObject.Find("Viewport").transform.Find("Text").GetComponent<Text>();
            Application.logMessageReceivedThreaded += UpdateLogBox;
            newLogs = new NativeList<char>(Allocator.Persistent);
        }

        private void UpdateLogBox(string condition, string stackTrace, LogType type) {
            string log = (type == LogType.Log || type == LogType.Warning)
                ? "\n This is a " + type + ": " + condition : "\n This is an " + type + ": " + condition;
            foreach (char buchstabe in log) 
                newLogs.Add(buchstabe);
        }

        private void LateUpdate() {
            if (newLogs.IsEmpty) 
                return;
            textObj.text += convert();
            newLogs.Clear();
            //while (textObj.isTextOverflowing) 
              //  textObj.text = textObj.text.Substring(textObj.text.IndexOf('\n') + 1);
        }

        private string convert() {
            StringBuilder builder = new StringBuilder(newLogs.Length);
            foreach(char logBuchstabe in newLogs)
                builder.Append(logBuchstabe);
            return builder.ToString();
        }
        
        public void changeTextBoxPosition(float newPos) {
            textObj.gameObject.GetComponent<RectTransform>().position += new Vector3(0, newPos, 0);
        }
        
        private void OnApplicationQuit() => newLogs.Dispose();
    }
}
﻿using Backend;
using UnityEngine;
using UnityEngine.UI;

namespace GUI_Skripte {
    public class RunningCheck : MonoBehaviour {
        
        public Image haken;

        private void Start() {
            if (haken != null && haken.gameObject.name.Equals("Checkmark")) 
                return;
            Debug.LogWarning("Haken Bild nicht gesetzt");
            haken = gameObject.transform.Find("Toggle").Find("Background").Find("Checkmark").gameObject.GetComponent<Image>();
        }

        private void Update() => haken.enabled = Backuper.getInstance().getTimer().Enabled;
        
        public void giveFeedback() {
            if (Backuper.getInstance().getTimer().Enabled)
                Debug.Log("Still running, don't worry.");
        }
    }
}
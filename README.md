# Autobackuper

Here you the src find the src of my Autobackuper. It was written in Unity for the frontend and C# for the backend. For the File Choose I used Windows Forms, which can easily be imported to Unity.
It can be used to automaticly make backups of files after a certain amount of time.

What this program does:
This very smal program simpoly checks if a given file has changed by storing and comparing the lastWriteTime of that file. If something chnaged it it, the file will be copy pasted, meaning the file content will be read and then written to another file in the same Directory with an addictional "Backup" in the fileName of the Backupfile.
The time, the program waits until the next check for changes can also be customized.
